package ga2tool;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.OutputStream;
import java.util.Random;

public class StreamTool {
  public static final void writePackedInt(int value, OutputStream o) throws IOException {
    if (value < 0) {
      System.err.println("[StreamTool#writePackedInt] invalid number: " + value);
      Thread.dumpStack();
      o.write(0);
    } else if (value <= 127) {
      o.write(value);
    } else if (value <= 16383) {
      o.write(value >> 7 | 0x80);
      o.write(value & 0x7F);
    } else if (value <= 2097151) {
      o.write(value >> 14 & 0x7F | 0x80);
      o.write(value >> 7 & 0x7F | 0x80);
      o.write(value & 0x7F);
    } else if (value <= 268435455) {
      o.write(value >> 21 & 0x7F | 0x80);
      o.write(value >> 14 & 0x7F | 0x80);
      o.write(value >> 7 & 0x7F | 0x80);
      o.write(value & 0x7F);
    } else {
      o.write(value >> 28 & 0x7F | 0x80);
      o.write(value >> 21 & 0x7F | 0x80);
      o.write(value >> 14 & 0x7F | 0x80);
      o.write(value >> 7 & 0x7F | 0x80);
      o.write(value & 0x7F);
    } 
  }
  
  public static final void writePackedInt(int value, ObjectOutput o) throws IOException {
    writePackedInt(value, (OutputStream)o);
  }
  
  public static final int readPackedInt(InputStream in) throws IOException {
    int value = 0;
    int v = in.read();
    if (v < 0)
      throw new EOFException(); 
    if ((v & 0x80) == 0)
      return v & 0x7F; 
    value = (v & 0x7F) << 7;
    v = in.read();
    if (v < 0)
      throw new EOFException(); 
    if ((v & 0x80) == 0)
      return value | v & 0x7F; 
    value = (value | v & 0x7F) << 7;
    v = in.read();
    if (v < 0)
      throw new EOFException(); 
    if ((v & 0x80) == 0)
      return value | v & 0x7F; 
    value = (value | v & 0x7F) << 7;
    v = in.read();
    if (v < 0)
      throw new EOFException(); 
    if ((v & 0x80) == 0)
      return value | v & 0x7F; 
    value = (value | v & 0x7F) << 7;
    v = in.read();
    if (v < 0)
      throw new EOFException(); 
    return value | v & 0x7F;
  }
  
  public static final int readPackedInt(ObjectInput in) throws IOException {
    return readPackedInt((InputStream)in);
  }
  
  public static final void writeSignedPackedInt(int value, OutputStream o) throws IOException {
    int abs = (value < 0) ? (-1 - value) : value;
    int sign = (value < 0) ? 64 : 0;
    if (abs < 63) {
      o.write(abs | sign);
    } else if (abs <= 8191) {
      o.write(abs >> 7 | 0x80 | sign);
      o.write(abs & 0x7F);
    } else if (abs <= 1048575) {
      o.write(abs >> 14 & 0x3F | 0x80 | sign);
      o.write(abs >> 7 & 0x7F | 0x80);
      o.write(abs & 0x7F);
    } else if (abs <= 134217727) {
      o.write(abs >> 21 & 0x3F | 0x80 | sign);
      o.write(abs >> 14 & 0x7F | 0x80);
      o.write(abs >> 7 & 0x7F | 0x80);
      o.write(abs & 0x7F);
    } else {
      o.write(abs >> 28 & 0x3F | 0x80 | sign);
      o.write(abs >> 21 & 0x7F | 0x80);
      o.write(abs >> 14 & 0x7F | 0x80);
      o.write(abs >> 7 & 0x7F | 0x80);
      o.write(abs & 0x7F);
    } 
  }
  
  public static final void writeSignedPackedInt(int value, ObjectOutput o) throws IOException {
    writeSignedPackedInt(value, (OutputStream)o);
  }
  
  public static final int readSignedPackedInt(InputStream in) throws IOException {
    int value = 0, mbit = 1, add = 0;
    int v = in.read();
    if (v < 0)
      throw new EOFException(); 
    if ((v & 0x40) != 0) {
      v &= 0xFFFFFFBF;
      mbit = -1;
      add = 1;
    } 
    if ((v & 0x80) == 0)
      return ((v & 0x7F) + add) * mbit; 
    value = (v & 0x7F) << 7;
    v = in.read();
    if (v < 0)
      throw new EOFException(); 
    if ((v & 0x80) == 0)
      return ((value | v & 0x7F) + add) * mbit; 
    value = (value | v & 0x7F) << 7;
    v = in.read();
    if (v < 0)
      throw new EOFException(); 
    if ((v & 0x80) == 0)
      return ((value | v & 0x7F) + add) * mbit; 
    value = (value | v & 0x7F) << 7;
    v = in.read();
    if (v < 0)
      throw new EOFException(); 
    if ((v & 0x80) == 0)
      return ((value | v & 0x7F) + add) * mbit; 
    value = (value | v & 0x7F) << 7;
    v = in.read();
    if (v < 0)
      throw new EOFException(); 
    return ((value | v & 0x7F) + add) * mbit;
  }
  
  public static final int readSignedPackedInt(ObjectInput in) throws IOException {
    return readSignedPackedInt((InputStream)in);
  }
  
  public static final int writeSignedPackedLong(long value, OutputStream o) throws IOException {
    long abs = (value < 0L) ? (-1L - value) : value;
    int sign = (value < 0L) ? 128 : 0;
    int c = 0;
    if (abs == 0L) {
      c = 0;
    } else if (abs <= 255L) {
      c = 1;
    } else if (abs <= 65535L) {
      c = 2;
    } else if (abs <= 16777215L) {
      c = 3;
    } else if (abs <= -1L) {
      c = 4;
    } else if (abs <= 1099511627775L) {
      c = 5;
    } else if (abs <= 281474976710655L) {
      c = 6;
    } else if (abs <= 72057594037927935L) {
      c = 7;
    } else {
      c = 8;
    } 
    o.write(c | sign);
    for (int l = 0; l < c; l++)
      o.write((int)(abs >> l * 8)); 
    return c;
  }
  
  public static final int writeSignedPackedLong(long value, ObjectOutput o) throws IOException {
    return writeSignedPackedLong(value, (OutputStream)o);
  }
  
  public static final long readSignedPackedLong(InputStream in) throws IOException {
    int mbit = 1, add = 0;
    long v = 0L;
    long ret = 0L;
    int c = in.read();
    if (c < 0)
      throw new EOFException(); 
    if (c == 0)
      return 0L; 
    if ((c & 0x80) != 0) {
      c &= 0xFFFFFF7F;
      mbit = -1;
      add = 1;
    } 
    for (int l = 0; l < c; l++) {
      v = in.read();
      if (v < 0L)
        throw new EOFException(); 
      ret |= v << l * 8;
    } 
    return (ret + add) * mbit;
  }
  
  public static final long readSignedPackedLong(ObjectInput in) throws IOException {
    return readSignedPackedLong((InputStream)in);
  }
  
  public static void main(String[] args) throws IOException {
    Random r = new Random();
    for (int l = 0; l < 100000; l++) {
      int i = r.nextInt();
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      writeSignedPackedInt(i, baos);
      ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
      int i2 = readSignedPackedInt(bais);
      System.out.println(i + " == " + i2 + "  " + ((i == i2) ? 1 : 0));
      if (i != i2)
        throw new RuntimeException("DOWA=======!!!!!"); 
    } 
  }

    public static String hexDump(ByteArrayOutputStream out) {
        String s = "";
        for (byte b: out.toByteArray()) {
            s = s + Integer.toHexString(b & 0xFF) + " ";
        }
        return s;
    }

    public static String decodePos(InputStream inputStream) {
        try {
            ObjectInputStream in = new ObjectInputStream(inputStream);
            Integer _i = in.readInt();
            Integer ix = _i & 0xFFFFF;
            Integer iy = in.readShort() & 0xFFFF;
            Integer iz = _i >>> 20 | (in.readByte() & 0xFF) << 12;
            Float x = (ix & 0x7FFFF) * 0.001F * (((ix & 0x80000) != 0) ? -1 : 1);
            Float y = (iy & 0x7FFF) * 0.002F * (((iy & 0x8000) != 0) ? -1 : 1);
            Float z = (iz & 0x7FFFF) * 0.001F * (((iz & 0x80000) != 0) ? -1 : 1);
            return "Position(" + x.toString() + ", " + y.toString() + ", " + z.toString() + ")";
        }
        catch (IOException e) {
            return "Invalid position: " + e.toString();
        }
    }

    public static String decodeVec32(InputStream inputStream){
        try {
            ObjectInputStream in = new ObjectInputStream(inputStream);
            int i = in.readInt();
            int i1 = i & 0x3FF;
            int i2 = i >>> 10 & 0xFFF;
            int i3 = i >>> 22 & 0x3FF;
            Float x = (i1 & 0x1FF) * 0.05F * (((i1 & 0x200) != 0) ? -1 : 1);
            Float y = (i2 & 0x7FF) * 0.05F * (((i2 & 0x800) != 0) ? -1 : 1);
            Float z = (i3 & 0x1FF) * 0.05F * (((i3 & 0x200) != 0) ? -1 : 1);
            return "Vector(" + x.toString() + ", " + y.toString() + ", " + z.toString() + ")";
        }
        catch (IOException e) {
            return "Invalid vector 32: " + e.toString();
        }
    }
}
