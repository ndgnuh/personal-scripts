package ga2tool;

import java.io.OutputStream;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.util.Scanner;
import java.util.InputMismatchException;

import ga2tool.StreamTool;

class Main {
    public static boolean running = true;
    public static String lastMsg = "";


    public static void main(String[] args) {

        while (running) {
            // clear everything
            clear();
            println(lastMsg);

            // menu printting
            println("==============================");
            println("1. Write packed signed int");
            println("2. Write packed unsigned int");
            println("3. Read packed signed int");
            println("4. Read packed unsigned int");
            println("5. Decode position");
            println("6. Decode vector32");
            println("99. Exit");

            // decision input
            int option = inputInt("Option: ");

            switch (option) {
                case 1:
                    lastMsg = "Result: " + writePacked(inputInt(), true);
                    break;
                case 2:
                    lastMsg = "Result: " + writePacked(inputInt(), false);
                    break;
                case 3:
                    lastMsg = readPacked(true);
                    break;
                case 4:
                    lastMsg = readPacked(false);
                    break;
                case 5:
                    lastMsg = StreamTool.decodePos(inputHex());
                    break;
                case 6:
                    lastMsg = StreamTool.decodeVec32(inputHex());
                    break;
                case 99:
                    lastMsg = "Quiting";
                    running = false;
                    break;
                default:
                    lastMsg = ("Invalid option!");
                    break;
            }
        }
    }

    public static void prompt(String prompt) {
    }

    public static void println(String s) {
        System.out.println(s);
    }

    public static void print(String s) {
        System.out.print(s);
    }

    public static void clear() {
        try {
            if (System.getProperty("os.name").contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } else {
                new ProcessBuilder("clear").inheritIO().start().waitFor();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // ==============================
    // Command line interface helpers
    // ==============================

    /* Validated integer reader */
    public static Integer inputInt() { return inputInt("Input a number: "); }
    public static Integer inputInt(String prompt) {
        Scanner s = new Scanner(System.in);
        Integer num = null;
        while (num == null) {
            print(prompt);
            try {
                num = s.nextInt();
            } catch (InputMismatchException e) {
                println("Invalid number");
            }
            s.nextLine();
        }
        return num;
    }


    /* Validated hex string reader */
    public static ByteArrayInputStream inputHex() { return inputHex("Input a hex string: "); }
    public static ByteArrayInputStream inputHex(String prompt) {
        Scanner s = new Scanner(System.in);
        String hex = null;
        ByteArrayInputStream stream = null;
        while (stream == null) {
            print(prompt);
            try {
                hex = s.nextLine();
                stream = createInputStreamFromHex(hex);
            } catch (Exception e) {
                println("Invalid hex string: " + e.toString());
            }
        }
        return stream;
    }

    /* create input for the read packed functions */
    public static ByteArrayInputStream createInputStreamFromHex(String hexString) {
        // Remove any spaces or non-hex characters from the string
        hexString = hexString.replaceAll("\\s", "");

        // Check if the hex string has an odd length
        if (hexString.length() % 2 != 0) {
            throw new IllegalArgumentException("Hex string must have an even length");
        }

        // Convert the hex string to a byte array
        byte[] byteArray = new byte[hexString.length() / 2];
        for (int i = 0; i < byteArray.length; i++) {
            int index = i * 2;
            int byteValue = Integer.parseInt(hexString.substring(index, index + 2), 16);
            byteArray[i] = (byte) byteValue;
        }

        // Create a new ByteArrayInputStream using the byte array
        return new ByteArrayInputStream(byteArray);
    }

    // wrapper for the read/write packed functions
    // ===========================================
    public static String writePacked(Integer num, boolean signed) { 
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            if (signed) {
                StreamTool.writeSignedPackedInt(num, stream);
            } else {
                StreamTool.writePackedInt(num, stream);
            }
            return StreamTool.hexDump(stream);
        } catch (Exception err) {
            return "(Error) " + err.toString();
        }
    }

    // read packed and report sizes
    public static String readPacked(boolean signed) { 
        ByteArrayInputStream in = inputHex();
        Integer n = in.available();
        String msg = "Result:";
        Integer num;
        while (n > 0) {
            try {
                if (signed) {
                    num = StreamTool.readSignedPackedInt(in);
                }else {
                    num = StreamTool.readPackedInt(in);
                }
                Integer length = n - in.available();
                msg += "\n* " + num.toString() + " - " + length.toString() + " byte(s)";
                n = in.available();
            }
            catch (Exception e) {
                msg += "\nError: " + e.toString();
                break;
            }
        }
        return msg;
    }
}
